import Head from "next/head"
import Script from "next/script"

const PageLayout = (props) => {
    return(
      <>
      <Head>
      <meta />
        <Script href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" strategy="beforeInteractive"/>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link href="https://fonts.googleapis.com/css2?family=Newsreader:ital,wght@0,600;1,600&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,500;0,600;0,700;1,300;1,500;1,600;1,700&display=swap" rel="stylesheet" />
        <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,400;1,400&display=swap" rel="stylesheet" />
        <link href="css/styles.css" rel="stylesheet" />
        <title>Rally App | Budgeting made easy for you!</title>
        <meta name="description" content="Rally App: Budgeting made easy! 
      Control your finances all in the palm of your hands. Available for Android and iOS."></meta>
      </Head>
      <div>
          {props.children}
      </div>
      </>
    )
}

export default PageLayout